package com.example.exam5;

import android.app.Activity;
import android.arch.lifecycle.ViewModel;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.ListAdapter;
import android.widget.TextView;

import java.util.List;

public class ContactAdapter extends BaseAdapter {
    private List<ContactModel> listContact;
    private Activity activity;
    public ContactAdapter(List<ContactModel> listContacts, Activity activity) {
        this.listContact = listContacts;
        this.activity = activity;
    }

    @Override
    public int getCount() {
        return listContact.size();
    }

    @Override
    public Object getItem(int podision) {
        return null;
    }

    @Override
    public long getItemId(int podision) {
        return 0;
    }

    @Override
    public View getView(int podision, View converView, ViewGroup parent) {
        if (converView ==null){
            LayoutInflater inflater = activity.getLayoutInflater();
            converView = inflater.inflate(R.layout.item_contact,parent,false);
            ViewHolder holder = new ViewHolder();
            holder.tvName = (TextView)converView.findViewById(R.id.tvName);
            holder.tvPhone = (TextView)converView.findViewById(R.id.tvPhone);
            holder.ivAvatar = (ImageView)converView.findViewById(R.id.ivAvatar);
            converView.setTag(holder);
        }
        ViewHolder holder = (ViewHolder)converView.getTag();
        ContactModel model = listContact.get(podision);
        holder.tvName.setText(model.getName());
        holder.tvPhone.setText(model.getPhone());
        holder.ivAvatar.setImageResource(model.getImage());
        return converView;
    }

    static class ViewHolder {
        TextView tvName;
        TextView tvPhone;
        ImageView ivAvatar;
    }
}
