package com.example.exam5;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Adapter;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.List;

public class MainActivity extends AppCompatActivity {

    private ListView lvContact;
    private List<ContactModel> listContacts = new ArrayList<>();
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        initData();
        lvContact = (ListView)findViewById(R.id.lvContact);
        ContactAdapter adapter = new ContactAdapter(listContacts,this);
        lvContact.setAdapter(adapter);

        lvContact.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int podision, long id) {
                ContactModel contactModel = listContacts.get(podision);
                Toast.makeText(MainActivity.this,contactModel.getName(),Toast.LENGTH_SHORT).show();
            }
        });

    }

    private void initData() {
        ContactModel contact = new ContactModel("Nguyen van 1","01245789",R.drawable.ic_launcher_background);
        listContacts.add(contact);
        contact = new ContactModel("Nguyen van 2","15478512",R.drawable.ic_launcher_background);
        listContacts.add(contact);
        contact = new ContactModel("Nguyen van 3","25485851",R.drawable.ic_launcher_background);
        listContacts.add(contact);
        contact = new ContactModel("Nguyen van 4","45785125",R.drawable.ic_launcher_background);
        listContacts.add(contact);
        contact = new ContactModel("Nguyen van 5","45785125",R.drawable.ic_launcher_background);
        listContacts.add(contact);
        contact = new ContactModel("Nguyen van 6","45785125",R.drawable.ic_launcher_background);
        listContacts.add(contact);
        contact = new ContactModel("Nguyen van 7","45785125",R.drawable.ic_launcher_background);
        listContacts.add(contact);
    }
}